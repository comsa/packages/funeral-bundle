<?php


namespace Comsa\FuneralBundle\EventListener;


use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Event\NewCommentEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class CommentSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $parameterBag;
    private $translator;
    private $twig;
    private $entityManager;
    private $session;

    public function __construct
    (
        \Swift_Mailer $mailer,
        ParameterBagInterface  $parameterBag,
        TranslatorInterface $translator,
        Environment $twig,
        EntityManagerInterface  $entityManager,
        SessionInterface $session
    )
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
          NewCommentEvent::NAME => "onNewComment"
        ];
    }

    public function onNewComment(NewCommentEvent $event)
    {
        $comment = $event->getComment();
        $this->mail($comment);
    }

    public function mail(Comment $comment)
    {
        $person = $comment->getPerson();
        $email = (new \Swift_Message())
            ->setFrom('info@uitvaartzorg-leo.be')
            ->setTo($person->getFamily())
            ->setSubject($this->translator->trans('comsa_funeral_bundle.new_comment'))
            ->setBody($this->twig->render('@Funeral/mail/build/new-comment.html.twig', [
                "comment" => $comment,
                "person" => $person
            ]), "text/html");

        $this->mailer->send($email);
    }
}
