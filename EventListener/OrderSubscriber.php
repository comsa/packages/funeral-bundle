<?php

namespace Comsa\FuneralBundle\EventListener;

use Comsa\FuneralBundle\Event\OrderConfirmedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Comsa\FuneralBundle\Entity\Order;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Swift_Message;


class OrderSubscriber implements EventSubscriberInterface
{
    private $mailer;
    private $parameterBag;
    private $translator;
    private $twig;
    private $entityManager;
    private $session;

    public function __construct
    (
        \Swift_Mailer $mailer,
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator,
        Environment $twig,
        EntityManagerInterface $entityManager,
        SessionInterface $session
    )
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            OrderConfirmedEvent::NAME => "onOrderConfirmed",
        ];
    }

    public function onOrderConfirmed(OrderConfirmedEvent $event)
    {
        $order = $event->getOrder();
        $this->mailCustomer($order);
        $this->mailAdmin($order);
    }

    protected function mailCustomer(Order $order)
    {
        $email = (new Swift_Message())
            ->setFrom($this->parameterBag->get("comsa_sulu_funeral_bundle_from_email"))
            ->setTo($order->getEmail())
            ->setSubject($this->translator->trans('comsa_funeral_bundle.thanks_for_order'))
            ->setBody($this->twig->render('@Funeral/mail/build/order-confirmation.html.twig', [
                "order" => $order
            ]), "text/html");

        $this->mailer->send($email);
    }

    protected function mailAdmin(Order $order)
    {
        $person = $order->getPerson();
        if (!empty($this->session->get("deliverer")))
        {
            $deliverer = $this->session->get("deliverer");
        }
        else
        {
            $deliverer = null;
        }

        $email = (new Swift_Message())
            ->setFrom($this->parameterBag->get("comsa_sulu_funeral_bundle_from_email"))
            ->setTo($this->parameterBag->get("comsa_sulu_funeral_bundle_new_order_email"))
            ->setSubject($this->translator->trans('comsa_funeral_bundle.new_order'))
            ->setBody($this->twig->render('@Funeral/mail/build/order-confirmation.html.twig', [
                "order" => $order,
                "deliverer" => $deliverer,
                "deliverDate" => $person->getFinalVisitDate()? $person->getFinalVisitDate()->format('Y-m-d') : "NVT"
            ]), "text/html");

        if ($this->session->get('sendOrderToFlorist') && $deliverer)
        {
            $email->setCc($deliverer);
        }

        $this->mailer->send($email);
    }
}
