<?php

namespace Comsa\FuneralBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class FuneralExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

    }

    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension('fos_js_routing')) {
            $container->prependExtensionConfig(
                'fos_js_routing',
                [
                    'routes_to_expose' => [
                        'comsa.funeral_bundle.get_people',
                        'comsa.funeral_bundle.get_person',
                        'comsa.funeral_bundle.export',
                        'comsa.funeral_bundle.export_email_addresses',
                        'comsa.funeral_bundle.put_person_pin',
                        'comsa.funeral_bundle.put_person_unpin'
                    ]
                ]
            );
        }

        if ($container->hasExtension('sulu_admin')) {
            $container->prependExtensionConfig(
                'sulu_admin',
                [
                    'lists' => [
                        'directories' => [
                            __DIR__ . '/../Resources/config/lists'
                        ]
                    ],
                    'forms' => [
                        'directories' => [
                            __DIR__ . '/../Resources/config/forms'
                        ]
                    ],
                    'resources' => [
                        'people' => [
                            'routes' => [
                                'list' => 'comsa.funeral_bundle.get_people',
                                'detail' => 'comsa.funeral_bundle.get_person'
                            ]
                        ],
                        'comments' => [
                            'routes' => [
                                'list' => 'comsa.funeral_bundle.get_comments',
                                'detail' => 'comsa.funeral_bundle.get_comment'
                            ]
                        ],
                        'orders' => [
                            'routes' => [
                                'list' => 'comsa.funeral_bundle.get_orders',
                                'detail' => 'comsa.funeral_bundle.get_order'
                            ]
                        ],
                    ],
                    'field_type_options' => [
                        'single_selection' => [
                            'single_person_selection' => [
                                'default_type' => 'list_overlay',
                                'resource_key' => 'people',
                                'types' => [
                                    'list_overlay' => [
                                        'adapter' => 'table',
                                        'list_key' => 'people',
                                        'display_properties' => [
                                            'fullName',
                                        ],
                                        'icon' => 'su-list-ul',
                                        'empty_text' => 'comsa_funeral_bundle.no_person_selected',
                                        'overlay_title' => 'Select your person'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            );
        }
    }
}
