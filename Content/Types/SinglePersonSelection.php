<?php

namespace Comsa\FuneralBundle\Content\Types;

use Comsa\FuneralBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Jackalope\Node;
USE PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;
use Sulu\Component\Content\PreResolvableContentTypeInterface;
use Sulu\Component\Content\SimpleContentType;

class SinglePersonSelection extends SimpleContentType
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        parent::__construct('single_person_selection', null);
    }


    public function getContentData(PropertyInterface $property)
    {
        $personId = $property->getValue();


        if (!$personId){
            return null;
        }

        $person = $this->entityManager->getRepository(Person::class)->find($personId);


        return $person;
    }

    public function getViewData(PropertyInterface $property)
    {
        return [
            'id' => $property->getValue()
        ];
    }
}
