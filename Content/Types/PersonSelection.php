<?php

namespace Comsa\FuneralBundle\Content\Types;

use Comsa\FuneralBundle\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Jackalope\Node;
USE PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;
use Sulu\Component\Content\PreResolvableContentTypeInterface;

class PersonSelection extends ComplexContentType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        $personId = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($personId);
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey)
    {
        $value = $property->getValue();

        if (null === $value){
            $node->setProperty($property->getName(), null);
            return;
        }

        foreach ($value as $person) {
            if (is_numeric($person)){
                $personId = $person;
            } else{
                $personId = $person['id'];
            }
        }

        $node->setProperty($property->getName(), $personId);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        if ($node->hasProperty($property->getName())){
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property)
    {
        $person = $property->getValue();

        $person = $this->entityManager->getRepository(Person::class)->find($person);

        return $person;
    }
}
