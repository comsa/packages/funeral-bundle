import './components/Person/index';
import {formToolbarActionRegistry} from 'sulu-admin-bundle/views/Form';
// import AbstractFormToolbarAction from 'sulu-admin-bundle/views/Form/ToolbarActions/AbstractFormToolbarAction';
import AbstractFormToolbarAction from '../../../../sulu/sulu/src/Sulu/Bundle/AdminBundle/Resources/js/views/Form/toolbarActions/AbstractFormToolbarAction';
import {translate} from 'sulu-admin-bundle/utils/Translator';
import router from 'sulu-admin-bundle/services/Router';
import {listToolbarActionRegistry} from 'sulu-admin-bundle/views';
import GetPeopleToolbarAction from "./listToolbarActions/GetPeopleToolbarAction";
import ExportComments from "./listToolbarActions/ExportComments";
import ExportEmailAddresses from "./listToolbarActions/ExportEmailAddresses";
import PinPeople from './listToolbarActions/PinPeople';
import UnpinPeople from './listToolbarActions/UnpinPeople';

listToolbarActionRegistry.add('comsa_funeral_bundle.get_people', GetPeopleToolbarAction);
listToolbarActionRegistry.add('comsa_funeral_bundle.export_comments', ExportComments);
listToolbarActionRegistry.add('comsa_funeral_bundle.export_email_addresses', ExportEmailAddresses);
listToolbarActionRegistry.add('comsa_funeral_bundle.pin_people', PinPeople);
listToolbarActionRegistry.add('comsa_funeral_bundle.unpin_people', UnpinPeople);

