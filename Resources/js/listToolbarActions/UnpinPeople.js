import {translate} from 'sulu-admin-bundle/utils';
import {AbstractListToolbarAction} from 'sulu-admin-bundle/views';
import symfonyRouting from 'fos-jsrouting/router';
import {observable} from "mobx";
import axios from "axios"

export default class pinPeople extends AbstractListToolbarAction {

  getToolbarItemConfig() {
    const {disable_for_empty_selection: disableForEmptySelection = true} = this.options;

    return {
      type: 'button',
      label: translate('comsa_funeral_bundle.unpin_people'),
      icon: 'su-map-pin',
      onClick: this.handleClick,
    };
  }

  handleClick = () => {
    var checkedValues = [];
    var markedCheckboxes = document.querySelectorAll('input[type="checkbox"]:checked');

    for (var i = 0; i < markedCheckboxes.length; i++)
    {
      checkedValues.push(markedCheckboxes[i].value);
    }

    const response = axios.put(symfonyRouting.generate('comsa.funeral_bundle.put_person_unpin', {ids: checkedValues.join("-")}));

    alert("Personen werden succesvol losgemaakt");

    window.location.reload();
  }
}
