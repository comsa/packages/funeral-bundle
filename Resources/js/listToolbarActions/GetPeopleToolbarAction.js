import {translate} from 'sulu-admin-bundle/utils';
import {AbstractListToolbarAction} from 'sulu-admin-bundle/views';

export default class GetPeopleToolbarAction extends AbstractListToolbarAction {
  getToolbarItemConfig() {
    const {disable_for_empty_selection: disableForEmptySelection = false} = this.options;

    return {
      type: 'button',
      label: translate('comsa_funeral_bundle.get_people'),
      icon: 'su-sync',
      onClick: this.handleClick,
    };
  }

  handleClick = () => {
    window.location.reload();
  }
}
