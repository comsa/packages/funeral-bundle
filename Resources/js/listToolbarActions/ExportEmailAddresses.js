import {translate} from 'sulu-admin-bundle/utils';
import {AbstractListToolbarAction} from 'sulu-admin-bundle/views';
import symfonyRouting from 'fos-jsrouting/router';

export default class exportComments extends AbstractListToolbarAction {
  getToolbarItemConfig() {
    const {disable_for_empty_selection: disableForEmptySelection = false} = this.options;

    return {
      type: 'button',
      label: translate('comsa_funeral_bundle.export_email_addresses'),
      icon: 'su-download',
      onClick: this.handleClick,
    };
  }

  handleClick = () => {
    window.location.replace(symfonyRouting.generate('comsa.funeral_bundle.export_email_addresses', { id: this.getId()}));
  }


  getId(){
    let url = window.location.href;
    let split = url.split("/");
    let id = split[7];

    return id;
  }


}
