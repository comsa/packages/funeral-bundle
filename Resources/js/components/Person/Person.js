import {observer} from 'mobx-react';
import {action,observable} from 'mobx';
import React from 'react';
import {ResourceRequester} from 'sulu-admin-bundle/services';
import {withToolbar} from 'sulu-admin-bundle/containers/Toolbar';
import Loader from 'sulu-admin-bundle/components/Loader';
import Section from 'sulu-admin-bundle/components/Form/Section'
import Card from 'sulu-admin-bundle/components/Card'

type Props = {
  ...ViewProps,
  title?: string,
};

class Person extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {
      person: null
    }
  }
  @action async componentDidMount() {
    const {router} = this.props;
    const person = await ResourceRequester.get('persons', {id: router.attributes.id});
    this.setState({
      person
    })
  }
  render() {
    const { person } = this.state;
    if (!person) {
      return <Loader/>
    }
    return (
      <div>
        <Section label="Persoon">
          <Card>
            <h1>Persoon xxxx</h1>
            <div><p>xxxx</p></div>
          </Card>
        </Section>
      </div>
    )
  }
}

export default withToolbar(Person, function() {
  return {};
});
