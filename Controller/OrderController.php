<?php


namespace Comsa\FuneralBundle\Controller;



use Comsa\FuneralBundle\Entity\Order;
use Comsa\FuneralBundle\Entity\Person;
use Comsa\FuneralBundle\Form\OrderType;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Psr\EventDispatcher\EventDispatcherInterface;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Comsa\FuneralBundle\Event\OrderConfirmedEvent;
use Swift_Message;

class OrderController extends AbstractController
{

    private $documentManager;
    private $entityManager;
    private $parameterBag;
    private $session;
    private $eventDispatcher;
    private $mailer;

    public function __construct(DocumentManagerInterface $documentManager, EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, SessionInterface $session, EventDispatcherInterface $eventDispatcher, \Swift_Mailer $mailer)
    {
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->session = $session;
        $this->eventDispatcher = $eventDispatcher;
        $this->mailer = $mailer;
    }

    public function renderOrderForm(string $uuid, Request $request)
    {
        $order = new Order();
        $person = "";

        if ($this->session->get('personId'))
        {
            $person = $this->entityManager->getRepository(Person::class)->find($this->session->get('personId'));
        }
        $product = $this->session->get('productName');

        if (!empty($person) || $person !== null){
            $form = $this->createForm(OrderType::class, $order, [
                'person' => $person,
                'product' => $product,
                'uuid' => $uuid
            ]);
        } else{
            $form = $this->createForm(OrderType::class, $order, [
                'product' => $product
            ]);
        }

        return $this->render('@Funeral/product.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    public function saveOrder(Request $request)
    {
        $order = new Order();
        $submittedOrder = $request->request->get('order');
        $product = $this->session->get('productName');
        $productImage = $this->session->get('productImage');
        $amount = (float) $this->session->get('amount');


        $order->setPerson($this->entityManager->getRepository(Person::class)->find($submittedOrder['person']));
        $order->setUuid($submittedOrder['uuid']);
        $order->setProduct($product);
        $order->setProductImage($productImage);
        $order->setAmount($amount);
        $order->setName($submittedOrder['name']);
        $order->setEmail($submittedOrder['email']);
        $order->setStreetAndNumber($submittedOrder['streetAndNumber']);
        $order->setPostalAndCity($submittedOrder['postalAndCity']);
        $order->setPhone($submittedOrder['phone']);
        $order->setCompanyNumber($submittedOrder['companyNumber']);
        $order->setAccessoryType($submittedOrder['accessoryType']);
        $order->setAccessoryText($submittedOrder['accessoryText']);
        $order->setInvoice(isset($submittedOrder['invoice']) ? $submittedOrder['invoice'] : 0);
        $order->setNotes($submittedOrder['notes']);

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        $this->eventDispatcher->dispatch(new OrderConfirmedEvent($order));

        $this->session->remove('personId');
        $this->session->remove('productName');
        $this->session->remove('productImage');
        $this->session->remove('deliverer');
        $this->session->remove('amount');
        $this->session->remove('sendOrderToFlorist');

        $this->addFlash("orderCompleted", "Uw bestelling voor <strong>". $order->getPerson()->getFullName() ."</strong> is succesvol verlopen.");

        return $this->redirect($request->headers->get('referer'));
    }
}
