<?php


namespace Comsa\FuneralBundle\Controller\Admin;


use Comsa\FuneralBundle\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OrderController extends AbstractRestController implements ClassResourceInterface
{
    private $listHelper;
    private $factory;
    private $fieldDescriptoryFactory;
    private $restHelper;
    private $entityManager;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        EntityManagerInterface $entityManager
    ){
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptoryFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->entityManager = $entityManager;
        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $this->getLocale($request);

        if (!$locale) {
            $locale = "nl";
        }

        $listBuilder = $this->factory->create(Order::class);
        $fieldDescriptors = $this->fieldDescriptoryFactory->getFieldDescriptors(Order::RESOURCE_KEY);
        $listBuilder->addSearchField($fieldDescriptors['name']);
        $listBuilder->addSearchField($fieldDescriptors['person']);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $list = $listBuilder->execute();

        $total = $listBuilder->count();
        $page = $listBuilder->getCurrentPage();
        $limit = $listBuilder->getLimit();

        $representation = new ListRepresentation(
              $list,
              "orders",
              $request->get("_route"),
              $request->query->all(),
            $page,
            $limit,
            $total
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Order $entity */
        $entity = $this->entityManager->getRepository(Order::class)->find($id);

        if (!$entity){
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($entity));
    }
}
