<?php

namespace Comsa\FuneralBundle\Controller\Admin;

use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Entity\Person;
use FOS\RestBundle\View\View;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;
use Dompdf\Dompdf;
use Dompdf\Options;

class CommentController extends AbstractRestController implements ClassResourceInterface
{
    private $listHelper;
    private $factory;
    private $fieldDescriptoryFactory;
    private $restHelper;
    private $entityManager;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        EntityManagerInterface $entityManager
    ){
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptoryFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->entityManager = $entityManager;
        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $this->getLocale($request);

        $id = $request->get('id');

        if (!$locale){
            $locale = "nl";
        }

        $fieldDescriptors = $this->fieldDescriptoryFactory->getFieldDescriptors(Comment::RESOURCE_KEY);
        $listBuilder = $this->factory->create(Comment::class);

        if (isset($fieldDescriptors['person'])){
            $listBuilder->where($fieldDescriptors['person'],  $id);
        }
        $listBuilder->addSearchField($fieldDescriptors['author']);
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $list = $listBuilder->execute();

        $total = $listBuilder->count();
        $page = $listBuilder->getCurrentPage();
        $limit = $listBuilder->getLimit();

        $representation = new ListRepresentation(
            $list,

            "comments",
            $request->get("_route"),
            $request->query->all(),
            $page,
            $limit,
            $total,
            $id
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Comment $entity */
        $entity = $this->entityManager->getRepository(Comment::class)->find($id);

        if (!$entity){
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($entity));
    }

    public function deleteAction(int $id, Request $request): Response
    {
        $comment = $this->entityManager->getRepository(Comment::class)->find($id);
        if (!$comment){
            throw new NotFoundHttpException();
        }

        $this->entityManager->remove($comment);
        $this->entityManager->flush();

        return $this->handleView($this->view());
    }
}
