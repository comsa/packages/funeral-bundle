<?php

namespace Comsa\FuneralBundle\Controller\Admin;

use Comsa\FuneralBundle\Entity\Person;
use Comsa\FuneralBundle\Manager\PersonManager;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Component\Rest\AbstractRestController;
use HandcraftedInTheAlps\RestRoutingBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
class PersonController extends AbstractRestController implements ClassResourceInterface
{
    private $listHelper;
    private $factory;
    private $fieldDescriptoryFactory;
    private $restHelper;
    private $entityManager;
    private $personManager;
    private $mediaManager;

    public function __construct
    (
        ViewHandlerInterface $viewHandler,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        RestHelperInterface $restHelper,
        EntityManagerInterface $entityManager,
        MediaManagerInterface $mediaManager,
        PersonManager $personManager
    ){
        $this->factory = $doctrineListBuilderFactory;
        $this->fieldDescriptoryFactory = $fieldDescriptorFactory;
        $this->restHelper = $restHelper;
        $this->entityManager = $entityManager;
        $this->mediaManager = $mediaManager;
        $this->personManager = $personManager;
        parent::__construct($viewHandler);
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $this->getLocale($request);

        if (!$locale){
            $locale = "nl";
        }

        $listBuilder = $this->factory->create(Person::class);
        $fieldDescriptors = $this->fieldDescriptoryFactory->getFieldDescriptors(Person::RESOURCE_KEY);
        $listBuilder->addSearchField($fieldDescriptors['fullName']);
        $listBuilder->sort($fieldDescriptors['isPinned'], 'DESC');
        $listBuilder->sort($fieldDescriptors['id'], 'DESC');
        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);
        $list = $listBuilder->execute();
        $total = $listBuilder->count();
        $page = $listBuilder->getCurrentPage();
        $limit = $listBuilder->getLimit();



        foreach ($list as $key => $person)
        {
            if ($list[$key]["isPinned"])
            {
                $list[$key]["isPinned"] = "Ja";
            }
        }


        $representation = new ListRepresentation(
            $list,
            "people",
            $request->get("_route"),
            $request->query->all(),
            $page,
            $limit,
            $total
        );



        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        /** @var Person $entity */
        $entity = $this->entityManager->getRepository(Person::class)->find($id);

        if (!$entity){
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($entity));
    }

    public function putAction(int $id, Request $request): Response
    {
        $person = $this->entityManager->find(Person::class, $id);
        if (!$person) {
            throw new NotFoundHttpException();
        }

        $person->setFinalSalute($request->request->get('finalSalute'));
        $person->setFuneralLocation($request->request->get('funeralLocation'));

        $pdf = $request->request->get('mourningLetter');


        if ($pdf){
            $pdfId = $pdf['id'];
            $person->setMourningLetter($this->mediaManager->getEntityById($pdfId));
        }
        $person->setLivestreamUrl($request->request->get('livestreamUrl'));
        $person->setFuneralDate(new \DateTime($request->request->get('funeralDate')));

        $this->entityManager->flush();

        return $this->handleView($this->view($person));
    }

    public function deleteAction(int $id, Request $request): Response
    {
        $person = $this->entityManager->getRepository(Person::class)->find($id);
        if (!$person){
            throw new NotFoundHttpException();
        }

        $this->entityManager->remove($person);
        $this->entityManager->flush();

        return $this->handleView($this->view());
    }

    public function putPinAction(Request $request): Response
    {
        $personRepository = $this->entityManager->getRepository(Person::class);
        $idsStr = $request->query->get('ids');
        $idsArr = explode('-', $idsStr);
        $people = [];

        foreach ($idsArr as $id)
        {
            /** @var Person $person */
            $person = $personRepository->find($id);
            $person->setIsPinned(true);
            $people[] = $person;
        }

        $this->entityManager->flush();

        return $this->handleView($this->view($people));
    }

    public function putUnpinAction(Request $request): Response
    {
        $personRepository = $this->entityManager->getRepository(Person::class);
        $idsStr = $request->query->get('ids');
        $idsArr = explode('-', $idsStr);
        $people = [];

        foreach ($idsArr as $id)
        {
            /** @var Person $person */
            $person = $personRepository->find($id);
            $person->setIsPinned(null);
            $people[] = $person;
        }

        $this->entityManager->flush();

        return $this->handleView($this->view($people));
    }
}
