<?php

namespace Comsa\FuneralBundle\Controller;

use Comsa\FuneralBundle\Entity\Person;
use Comsa\FuneralBundle\Event\NewCommentEvent;
use Comsa\FuneralBundle\Form\CommentType;
use Comsa\FuneralBundle\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Dompdf\Dompdf;
use Dompdf\Options;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Psr\EventDispatcher\EventDispatcherInterface;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Dompdf\FontMetrics;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class CommentController extends AbstractController
{

    private $documentManager;
    private $entityManager;
    private $parameterBag;
    private $eventDispatcher;

    public function __construct(DocumentManagerInterface $documentManager, EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, EventDispatcherInterface $eventDispatcher)
    {
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function save(Request $request)
    {
        $comment = new Comment();

        /**
         * @var Person $person
         */
        $person = $this->entityManager->getRepository(Person::class)->find($request->request->get('personId'));

        $comment->setPerson($person);
        $comment->setAuthor($request->request->get('author'));
        $comment->setAddress($request->request->get('address'));
        $comment->setPhone($request->request->get('phone'));
        $comment->setEmail($request->request->get('email'));
        $comment->setText(utf8_encode($request->request->get('text')));
        $comment->setCreatedAt(new \DateTime());

        if ($photo = $request->files->get('formUpload')){
            $filename = bin2hex(random_bytes(6)) . '.' . $photo->guessExtension();
            try {
                $photo->move($this->parameterBag->get('comsa_sulu_funeral_bundle_photo_directory'), $filename);
            } catch (FileException $e){

            }

            $comment->setPhotoFileName($filename);
        }

        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        $today = new \DateTime();

        if
        (
            $today >= $person->getMailFamilyDate()
            &&
            $today <= $today->modify('+14 days')
        )
        {
            $this->eventDispatcher->dispatch(new NewCommentEvent($comment));
        }

        $this->addFlash('commentAdded', "Uw condolatie <strong>" . $comment->getPerson()->getFullName() . "</strong> werd correct opgeslaan.");

        return $this->redirect($request->headers->get('referer'));

    }

    public function export(Request $request)
    {

        $id = $request->get('id');
        $person = $this->entityManager->getRepository(Person::class)->find($id);
        $comments = $person->getComments();
        $date = new \DateTime();


        $dompdf = new Dompdf();
        $options = $dompdf->getOptions();

        //-- Enables local file access
        $options->setIsRemoteEnabled(TRUE);
        $options->setChroot($this->getParameter('kernel.project_dir'));
        $options->setIsFontSubsettingEnabled(true);
        $options->setIsPhpEnabled(true);


        $totalComments = count($comments);
        $totalPages = ceil($totalComments/5);

        $dompdf->setOptions($options);
        $html = $this->renderView('@Funeral\comments.html.twig', [
            'person' => $person,
            'comments' => $comments,
            'date' => $date->format('d-m-Y'),
        ]);

        $dompdf->setPaper('A4', 'portrait');
        $dompdf->loadHtml($html);
        $dompdf->render();

        $font = new FontMetrics($dompdf->getCanvas(), $dompdf->getOptions());
        $font = $font->getFont("helvica", "bold");
        $dompdf->getCanvas()->page_text(510, 775, "Pagina: {PAGE_NUM} van {PAGE_COUNT}", $font, 9, [0,0,0]);
        $dompdf->stream("Condolaties-" . $person->getFullName() . "-" . $date->format('d-m-Y'));



        return new Response();
    }

    public function exportEmailAddresses(Request $request): Response
    {
        $id = $request->get('id');
        /** @var Person $person */
        $person = $this->entityManager->getRepository(Person::class)->find($id);
        $comments = $person->getComments();

        $spreadSheet = new Spreadsheet();
        $sheet = $spreadSheet->getActiveSheet();

        $sheet->setTitle('mails');
        $x = 1;
        /** @var Comment $comment */
        foreach ($comments as $comment)
        {
            $sheet->setCellValue('A'.$x, $comment->getEmail());
            $x++;
        }

        $writer = new Xlsx($spreadSheet);
        $fileName = "email-adressen-".$person->getFullName() . '.xlsx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName);
    }

}
