<?php

namespace Comsa\FuneralBundle\Controller;

use Comsa\FuneralBundle\Entity\Person;
use Comsa\FuneralBundle\Form\CommentType;
use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Repository\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Component\DocumentManager\DocumentManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PersonController extends AbstractController
{

    private $documentManager;
    private $entityManager;
    private $parameterBag;
    private $mediaManager;

    public function __construct(DocumentManagerInterface $documentManager, EntityManagerInterface $entityManager, ParameterBagInterface $parameterBag, MediaManagerInterface $mediaManager)
    {
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->parameterBag = $parameterBag;
        $this->mediaManager = $mediaManager;
    }

    public function renderPerson(string $uuid, Request $request): Response
    {

        $document = $this->documentManager->find($uuid);
        $person = $document->getStructure()->getContentViewProperty('person')->getValue();
        if ($person){
            $comments = $person->getComments();
            $comment = new Comment();
            $form = $this->createForm(CommentType::class, $comment);
            return $this->render('@Funeral/person.html.twig', [
                'form' => $form->createView(),
                'person' => $person,
                'comments' => $comments
            ]);
        }

        return $this->render('@Funeral/person.html.twig', []);


    }

    public function renderHomePeople(string $uuid, Request $request): Response
    {
        $document = $this->documentManager->find($uuid);
        $people = $this->entityManager->getRepository(Person::class)->findLastThree();

        return $this->render('@Funeral/peopleHome.html.twig',
        [
           'people' => $people,
        ]);
    }

    public function renderPeople(string $uuid, Request $request): Response
    {
        $document = $this->documentManager->find($uuid);
        $people = $this->entityManager->getRepository(Person::class)->findAllOrderDesc();

        return $this->render('@Funeral/people.html.twig',[
           'people' => $people,
        ]);
    }
}
