<?php

namespace Comsa\FuneralBundle\Form;

use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Entity\Person;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, [
                'label' => 'comsa_funeral_bundle.author',
            ])
            ->add('address', TextType::class, [
                'label' => 'comsa_funeral_bundle.address'
            ])
            ->add('phone', TextType::class, [
                'label' => 'comsa_funeral_bundle.phone'
            ])
            ->add('email', EmailType::class, [
                'label' => 'comsa_funeral_bundle.email'
            ])
            ->add('text', TextareaType::class, [
                'label' => 'comsa_funeral_bundle.comment'
            ])
            ->add('photo', FileType::class, [
                'label' => 'comsa_funeral_bundle.photo' ,
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new Image(['maxSize' => '1024k'])
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'comsa_funeral_bundle.save_comment'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
