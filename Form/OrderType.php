<?php


namespace Comsa\FuneralBundle\Form;

use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Entity\Order;
use Comsa\FuneralBundle\Entity\Person;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (!empty($options['person'])){
            $builder
                ->add('person', EntityType::class, [
                    'class' => Person::class,
                    'choice_label' => 'fullName',
                    'data' => $options['person'],
                    'label' => false,
                ]);
        } else{
            $builder
                ->add('person', EntityType::class, [
                    'class' => Person::class,
                    'query_builder' => function (EntityRepository $entityRepository) {
                        return $entityRepository->createQueryBuilder('p')
                            ->Where('p.finalVisitDate > :date')
                            ->orWhere("p.finalVisitDate is null")
                            ->setParameter('date', date('Y-m-d'));
                    },
                    'choice_label' => 'fullName',
                    'label' => false,
                ]);
        }
        $builder
            ->add('product', HiddenType::class, [
                'data' => $options['product']
            ])
            ->add('uuid', HiddenType::class, [
                'data' => $options['uuid']
            ])
            ->add('name', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.name'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.email'
                ]
            ])
            ->add('streetAndNumber', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.street_and_number'
                ]
            ])
            ->add('postalAndCity', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.postal_and_city'
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.phone'
                ]
            ])
            ->add('companyNumber', TextType::class, [
                'label' => false,
                'required' => false ,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.company_number'
                ]
            ])
            ->add('accessoryType', ChoiceType::class, [
                "label" => false,
                "choices" => [
                    "Kaart toevoegen" => Order::ACCESSORY_CARD,
                    "Lint toevoegen" => Order::ACCESSORY_RIBBON
                ],
                "required" => false,
                "multiple" => false,
                'placeholder' => "Kaart of Lint toevoegen"
            ])
            ->add('accessoryText', TextType::class, [
                'label' => false,
                'required' => false,
                "attr" => [
                    'placeholder' => 'comsa_funeral_bundle.card'
                ],
            ])
            ->add('invoice', CheckboxType::class, [
                'required' => false,
                'label' => 'comsa_funeral_bundle.invoice',
            ])
            ->add('notes', TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'comsa_funeral_bundle.note'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'comsa_funeral_bundle.order'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);

        $resolver->setRequired([
           'person',
            'product',
            'uuid',
        ]);
    }
}
