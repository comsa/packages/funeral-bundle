<?php

namespace Comsa\FuneralBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Comsa\FuneralBundle\Manager\PersonManager;
use Symfony\Component\Validator\Constraints\DateTime;

class ImportPeopleCommand extends Command
{
    protected static $defaultName = 'comsa:funeral:import';

    private $personManager;

    public function __construct(PersonManager $personManager)
    {
        $this->personManager = $personManager;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription("Import people");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->personManager->getPeople();
        $date = new \DateTime();
        $output->writeln("<info>DONE {$date->format('Y-m-d H:i')}</info>");
        return Command::SUCCESS;
    }
}
