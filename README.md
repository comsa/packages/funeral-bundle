# Installation
```bash
composer require comsa/sulu-funeral-bundle
```

Add to `assets/admin/package.json`:

```json
  "sulu-funeral-bundle": "file:../../vendor/comsa/sulu-funeral-bundle/Resources/js"
```

Run `npm install`

Add it to `index.js`:

```js
import 'sulu-funeral-bundle/admin'
```

And build it using `npm run build`, this might take a while :)

Setup the frontend js:
Add it to package.json:
```json
"sulu-funeral-bundle": "file:../../vendor/comsa/sulu-funeral-bundle/Resources/js"
```
Add it to index.js:
```js
import 'sulu-funeral-bundle/website';
```
And build it using `npm run build` or use `npm run dev` in development

Add routes to both `routes_admin.yaml` and `routes_website.yaml`

In: config/routes_admin.yaml
``` yaml
comsa_funeral_api:
  type: rest
  resource: "@FuneralBundle/Resources/config/routes/admin.yaml"
  prefix: /admin/api
comsa_funeral_admin_controller:
  resource: "@FuneralBundle/Resources/config/routes/adminController.yaml"
``` 

In: config/routes_website.yaml
```yaml
comsa_funeral:
  resource: "@FuneralBundle/Resources/config/routes/website.yaml"
  prefix: /sulu-funeral
```

Add a field to your product template with key *person*:

```xml
 <property name="person" type="person_selection">
    <meta>
        <title lang="en">Person</title>
        <title lang="nl">Persoon</title>
    </meta>
</property>
```

Render person pages:

```twig
{{ render(controller('Comsa\\FuneralBundle\\Controller\\PersonController::renderPerson', {'uuid': id })) }}
```

Update your database to add the needed tables using `php bin/console doctrine:schema:update -f`

# Default configuration
Adjust to your wishes
```yaml
parameters:
  comsa_sulu_funeral_bundle_base_url: 'https://uitvaartzorgleo.funeralmanager.rip/'
    comsa_sulu_funeral_bundle_api_link: "https://uitvaartzorgleo.funeralmanager.rip/api.php/defunt?pompes_funebres_id=4f2fdd56dcc7a7.43283518&days=60"
    comsa_sulu_funeral_bundle_auth_key:  '%env(AUTH_KEY)%'
    comsa_sulu_funeral_bundle_photo_directory: "%kernel.project_dir%/public/uploads/photos"
    comsa_sulu_funeral_bundle_from_email: "tommy@comsa.be"
    comsa_sulu_funeral_bundle_new_order_email: "tommy@comsa.be"
  
```
We recommend using Symfony secrets for declaring sensitive data. <br>
Generate keys for development environment and/or production environment:

```bash
symfony console secrets:generate-keys 
```

Or in production environment:
```bash
symfony console secrets:generate-keys --env=prod
```

Define your secrets:

```bash
symfony console secrets:set AUTH_KEY
```
Or for production environment
```bash
symfony console secrets:set AUTH_KEY --env=prod
```
# Pages

Person page:
```twig
{{ render(controller('Comsa\\FuneralBundle\\Controller\\PersonController::renderPerson', {'uuid': id })) }}
```

People with limit of 3
```twig
{{ render(controller('Comsa\\FuneralBundle\\Controller\\PersonController::renderHomePeople', {'uuid': id })) }}
```

All People:
```twig
{{ render(controller('Comsa\\FuneralBundle\\Controller\\PersonController::renderPeople', {'uuid': id })) }}
```

Order Page:
```twig
{{ render(controller('Comsa\\FuneralBundle\\Controller\\OrderController::renderOrderForm')) }}
```

Comment Form: 
```html
<form class="col-md-10 offset-md-1 mourning-form hide" enctype="multipart/form-data" method="post" action="{{ path('comsa.funeral_bundle.save') }}">
    <div class="form-row">
        <div class="col-md-6">
            <input type="text" name="personId" hidden value="{{ person.id }}">
            <div class="form-group">
                <input type="text" name="author" class="form-control" placeholder="Naam">
            </div>
            <div class="form-row form-group">
                <div class="col">
                    <input type="email" name="email" class="form-control" placeholder="Email">
                </div>
                <div class="col">
                    <input type="tel" name="phone" class="form-control" placeholder="Telefoon">
                </div>
            </div>
            <div class="form-group">
                <input type="text" name="address" class="form-control" placeholder="Volledig Adres">
            </div>
            <div class="form-group">
                <input type="file" name="formUpload" class="form-control-file">
            </div>
        </div>
        <div class="col-md-6">
            <textarea class="form-control" name="text" placeholder="Bericht"
                      rows="6"></textarea>
            <button type="submit" class="btn btn-primary">Verzenden</button>
        </div>
    </div>
</form>
```

#Notes
Logos used in pdf and mail: <br>
<u>PDF:</u> <b>public/uploads/media/logoPdf.png</b> <br>
<u>Mail:</u> <b>public/uploads/media/logoMail.png</b>






