<?php


namespace Comsa\FuneralBundle\Event;


use Comsa\FuneralBundle\Entity\Comment;
use Symfony\Contracts\EventDispatcher\Event;

class NewCommentEvent extends Event
{
    CONST NAME = "new.comment";

    /**
     * @var Comment $comment
     */
    private $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function getComment(): Comment
    {
        return $this->comment;
    }
}
