<?php

namespace Comsa\FuneralBundle\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Comsa\FuneralBundle\Entity\Order;

class OrderConfirmedEvent extends Event
{
    CONST NAME = "order.confirmed";

    /**
     * @var Order $order
     */
    private $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    public function getOrder(): Order
    {
        return $this->order;
    }
}
