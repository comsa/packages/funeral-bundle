<?php

namespace Comsa\FuneralBundle\Repository;

use Comsa\FuneralBundle\Entity\Person;
use Doctrine\ORM\EntityRepository;

class PersonRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy([], ["isPinned" => "DESC", "id" => "ASC"]);
    }

    public function findAllOrderDesc()
    {
        return $this->createQueryBuilder("p")
            ->addOrderBy("p.isPinned", "DESC")
            ->addOrderBy("p.dateDeath", "DESC")
            ->setMaxResults(30)
            ->getQuery()
            ->getResult();
    }

    public function findLastThree()
    {
        return $this->createQueryBuilder("p")
            ->addOrderBy('p.isPinned', 'DESC')
            ->addOrderBy("p.dateDeath", "DESC")
            ->setMaxResults(3)
            ->getQuery()
            ->getResult();
    }


}
