<?php


namespace Comsa\FuneralBundle\Admin;


use Comsa\FuneralBundle\Entity\Order;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

class OrderAdmin extends Admin
{
    const ORDER_LIST_VIEW = 'comsa_funeral_bundle.orders_list';
    const ORDER_LIST_KEY = 'orders';
    const ORDER_EDIT_VIEW = 'comsa_funeral_bundle.order_detail';
    const ORDER_FORM_KEY = 'order_details';

    /**
     * @var ViewBuilderFactoryInterface
     */
    private $viewBuilderFactory;

    /**
     * @var WebspaceManagerInterface
     */
    private $webspaceManager;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory, WebspaceManagerInterface $webspaceManager)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->webspaceManager = $webspaceManager;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $module = new NavigationItem('comsa_funeral_bundle.orders');
        $module->setIcon('su-dollar');
        $module->setView(static::ORDER_LIST_VIEW);

        $navigationItemCollection->add($module);
    }

    private function configureOrderViews(ViewCollection $viewCollection): void
    {
        $locales = $this->webspaceManager->getAllLocales();
        $orderListView = $this->viewBuilderFactory->createListViewBuilder(static::ORDER_LIST_VIEW, '/orders/:locale')
            ->setResourceKey(Order::RESOURCE_KEY)
            ->setListKey(static::ORDER_LIST_KEY)
            ->setTitle('comsa_funeral_bundle.orders')
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->setEditView(static::ORDER_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.delete')
            ])
            ->addListAdapters(['table']);

        $viewCollection->add($orderListView);

        $viewCollection->add($this->viewBuilderFactory->createResourceTabViewBuilder(static::ORDER_EDIT_VIEW, '/orders/:locale/:id')
            ->setResourceKey(Order::RESOURCE_KEY)
            ->setBackView(static::ORDER_LIST_VIEW)
            ->setTitleProperty('title')
            ->addLocales($locales)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::ORDER_EDIT_VIEW . '.details', '/details')
            ->setResourceKey(Order::RESOURCE_KEY)
            ->setFormKey(static::ORDER_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.delete')
            ])
            ->setParent(static::ORDER_EDIT_VIEW)
        );

    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $this->configureOrderViews($viewCollection);
    }
}
