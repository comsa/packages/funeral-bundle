<?php

namespace Comsa\FuneralBundle\Admin;

use Comsa\FuneralBundle\Entity\Comment;
use Comsa\FuneralBundle\Entity\Person;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;


class PersonAdmin extends Admin
{
    const PERSON_LIST_VIEW = 'comsa_funeral_bundle.people_list';
    const PERSON_LIST_KEY = 'people';
    const PERSON_EDIT_VIEW = 'comsa_funeral_bundle.person_detail';
    const PERSON_FORM_KEY = 'person_details';
    const PERSON_ADD_VIEW = 'comsa_funeral_bundle.person_add';

    const COMMENT_LIST_VIEW = 'comsa_funeral_bundle.comments_list';
    const COMMENT_LIST_KEY = 'comments';

    /**
     * @var ViewBuilderFactoryInterface
     */
    private $viewBuilderFactory;

    /**
     * @var WebspaceManagerInterface
     */
    private $webspaceManager;

    public function __construct(ViewBuilderFactoryInterface $viewBuilderFactory, WebspaceManagerInterface $webspaceManager)
    {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->webspaceManager = $webspaceManager;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $module = new NavigationItem('comsa_funeral_bundle.people');
        $module->setIcon('su-user');
        $module->setView(static::PERSON_LIST_VIEW);

        $navigationItemCollection->add($module);
    }

    private function configurePersonViews(ViewCollection $viewCollection): void
    {
        $locales = $this->webspaceManager->getAllLocales();
        $personListView = $this->viewBuilderFactory->createListViewBuilder(static::PERSON_LIST_VIEW, '/people/:locale')
            ->setResourceKey(Person::RESOURCE_KEY)
            ->setListKey(static::PERSON_LIST_KEY)
            ->setTitle('comsa_funeral_bundle.people')
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->setAddView(static::PERSON_ADD_VIEW)
            ->setEditView(static::PERSON_EDIT_VIEW)
            ->addToolbarActions([
                new ToolbarAction('comsa_funeral_bundle.get_people'),
                new ToolbarAction('sulu_admin.delete'),
                new ToolbarAction('comsa_funeral_bundle.pin_people'),
                new ToolbarAction('comsa_funeral_bundle.unpin_people')
            ])
            ->addListAdapters(['table']);

        $viewCollection->add($personListView);


        $viewCollection->add($this->viewBuilderFactory->createResourceTabViewBuilder(static::PERSON_EDIT_VIEW, '/people/:locale/:id')
            ->setResourceKey(Person::RESOURCE_KEY)
            ->setBackView(static::PERSON_LIST_VIEW)
            ->setTitleProperty('title')
            ->addLocales($locales)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::PERSON_EDIT_VIEW . '.details', '/details')
            ->setResourceKey(Person::RESOURCE_KEY)
            ->setFormKey(static::PERSON_FORM_KEY)
            ->setTabTitle('sulu_admin.details')
            ->addToolbarActions([
                new ToolbarAction('sulu_admin.save'),
                new ToolbarAction('sulu_admin.delete')
            ])
            ->setParent(static::PERSON_EDIT_VIEW)
        );

        $viewCollection->add($this->viewBuilderFactory->createListViewBuilder(static::PERSON_EDIT_VIEW. '.comments', '/comments')
            ->setResourceKey(Comment::RESOURCE_KEY)
            ->setListKey(static::COMMENT_LIST_KEY)
            ->setTitle('comsa_funeral_bundle.comments')
            ->setTabTitle('comsa_funeral_bundle.comments')
            ->addListAdapters(['table'])
            ->addRouterAttributesToListRequest(['id' => 'id'])
            ->addResourceStorePropertiesToListRequest(['id' => 'id'])

            ->addToolbarActions([
                new ToolbarAction('sulu_admin.delete'),
                new ToolbarAction('comsa_funeral_bundle.export_comments'),
                new ToolbarAction('comsa_funeral_bundle.export_email_addresses'),
                ])
            ->setParent(static::PERSON_EDIT_VIEW)
        );
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $this->configurePersonViews($viewCollection);
    }
}
