<?php

namespace Comsa\FuneralBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Sulu\Bundle\MediaBundle\Entity\MediaInterface;

/**
 * @ORM\Entity(repositoryClass="Comsa\FuneralBundle\Repository\PersonRepository")
 * @ORM\Table(name="comsa_funeral_person")
 */
class Person
{
    const RESOURCE_KEY = 'people';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partnerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $civilStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $civilStatusFormat;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $civilStatusList;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dateBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placeBirth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dateDeath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timeDeath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placeDeath;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $finalSalute;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $funeralLocation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $funeralDate;

    /**
     * @var $finalVisitDate
     * @ORM\Column(type="date", nullable=true)
     */
    private $finalVisitDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $modifiedAt;

    /**
     * @var Comment[]
     * @ORM\OneToMany(targetEntity="Comsa\FuneralBundle\Entity\Comment", mappedBy="person", cascade={"persist", "remove"})
     * @Serializer\Exclude()
     */
    private $comments;


    //--For manual PDF upload:
//    /**
//     * @ORM\OneToOne(targetEntity="Sulu\Bundle\MediaBundle\Entity\MediaInterface")
//     * @ORM\JoinColumn(onDelete="SET NULL")
//     */
//    private ?MediaInterface $mourningLetter = null;

    //--For API pdf:
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mourningLetter;

    /**
     * @ORM\Column(type="text", length=65535, nullable=true)
     */
    private $livestreamUrl;

    /**
     * @ORM\Column(type="text", length=65535, nullable=true)
     */
    private $note;

    /**
     * @var $family
     * @ORM\Column(name="family", type="array", nullable=true)
     */
    private $family;

    /**
     * @var $mailFamilyDate
     * @ORM\Column(type="date", nullable=true)
     */
    private $mailFamilyDate;

    /**
     * @var Order[]
     * @ORM\OneToMany(targetEntity="Comsa\FuneralBundle\Entity\Order", mappedBy="person", cascade={"persist", "remove"})
     * @Serializer\Exclude()
     */
    private $orders;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPinned;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): void
    {
        $this->photo = $photo;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    public function getSecondName(): ?string
    {
        return $this->secondName;
    }

    public function setSecondName(?string $secondName): void
    {
        $this->secondName = $secondName;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): void
    {
        $this->gender = $gender;
    }

    public function getPartnerName(): ?string
    {
        return $this->partnerName;
    }

    public function setPartnerName(?string $partnerName): void
    {
        $this->partnerName = $partnerName;
    }

    public function getCivilStatus(): ?string
    {
        return $this->civilStatus;
    }

    public function setCivilStatus(?string $civilStatus): void
    {
        $this->civilStatus = $civilStatus;
    }

    public function getCivilStatusFormat(): ?string
    {
        return $this->civilStatusFormat;
    }

    public function setCivilStatusFormat(?string $civilStatusFormat): void
    {
        $this->civilStatusFormat = $civilStatusFormat;
    }

    public function getCivilStatusList()
    {
        return $this->civilStatusList;
    }

    public function setCivilStatusList($civilStatusList)
    {
        $this->civilStatusList = $civilStatusList;
    }

    public function getDateBirth(): ?string
    {
        return $this->dateBirth;
    }

    public function setDateBirth($dateBirth): void
    {
        $this->dateBirth = $dateBirth;
    }

    public function getPlaceBirth(): ?string
    {
        return $this->placeBirth;
    }

    public function setPlaceBirth($placeBirth): void
    {
        $this->placeBirth = $placeBirth;
    }

    public function getDateDeath(): ?string
    {
        return $this->dateDeath;
    }

    public function setDateDeath($dateDeath): void
    {
        $this->dateDeath = $dateDeath;
    }

    public function getTimeDeath(): ?string
    {
        return $this->timeDeath;
    }

    public function setTimeDeath($timeDeath): void
    {
        $this->timeDeath = $timeDeath;
    }

    public function getPlaceDeath(): ?string
    {
        return $this->placeDeath;
    }

    public function setPlaceDeath($placeDeath): void
    {
        $this->placeDeath = $placeDeath;
    }

    public function getFinalSalute(): ?string
    {
        return $this->finalSalute;
    }

    public function setFinalSalute($finalSalute): void
    {
        $this->finalSalute = $finalSalute;
    }

    public function getFinalVisitDate()
    {
        return $this->finalVisitDate;
    }

    public function setFinalVisitDate($finalVisitDate)
    {
        $this->finalVisitDate = $finalVisitDate;
    }

    public function getFuneralLocation(): ?string
    {
        return $this->funeralLocation;
    }

    public function setFuneralLocation($funeralLocation): void
    {
        $this->funeralLocation = $funeralLocation;
    }


    public function getFuneralDate(): ?\DateTime
    {
        return $this->funeralDate;
    }

    public function setFuneralDate($funeralDate): void
    {
        $this->funeralDate = $funeralDate;
    }


    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getModifiedAt(): \DateTime
    {
        return $this->modifiedAt;
    }

    public function setModifiedAt($modifiedAt): void
    {
        $this->modifiedAt = $modifiedAt;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)){
            $this->comments[] = $comment;
            $comment->setPerson($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)){
            $this->comments->removeElement($comment);
            if ($comment->getPerson() === $this ){
                $comment->setPerson(null);
            }
        }

        return $this;
    }

    //-- For Manual PDF upload
//    public function getMourningLetter(): ?MediaInterface
//    {
//        return $this->mourningLetter;
//    }

    //--For API PDF
    public function getMourningLetter(): ?string
    {
        return $this->mourningLetter;
    }

    //-- For Manual PDF upload
//    public function setMourningLetter(?MediaInterface $mourningLetter): void
//    {
//        $this->mourningLetter = $mourningLetter;
//    }

    //--For API PDF
    public function setMourningLetter(?string $mourningLetter): void
    {
        $this->mourningLetter = $mourningLetter;
    }

    //-- For Manual PDF upload
//    /**
//     * @return array<string, mixed>
//     * @Serializer\VirtualProperty()
//     * @Serializer\SerializedName("mourningLetter")
//     */
//    public function getMourningLetterData(): ?array
//    {
//        if ($mourningLetter = $this->getMourningLetter()){
//            return [
//                'id' => $mourningLetter->getId(),
//            ];
//        }
//
//        return null;
//    }

    public function getLivestreamUrl(): ?string
    {
        return $this->livestreamUrl;
    }

    public function setLivestreamUrl(?string $livestreamUrl): void
    {
        $this->livestreamUrl = $livestreamUrl;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note)
    {
        $this->note = $note;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function setFamily($family)
    {
        $this->family = $family;
    }

    public function getMailFamilyDate()
    {
        return $this->mailFamilyDate;
    }

    public function setMailFamilyDate($mailFamilyDate)
    {
        $this->mailFamilyDate = $mailFamilyDate;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)){
            $this->orders[] = $order;
            $order->setPerson($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)){
            $this->orders->removeElement($order);
            if ($order->getPerson() === $this ){
                $order->setPerson(null);
            }
        }

        return $this;
    }

    public function getIsPinned(): bool
    {
        return $this->isPinned;
    }

    public function setIsPinned(?bool $isPinned):  void
    {
        $this->isPinned = $isPinned;
    }

}
