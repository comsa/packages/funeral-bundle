<?php


namespace Comsa\FuneralBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_funeral_orders")
 */
class Order
{
    const RESOURCE_KEY = 'orders';
    const ACCESSORY_RIBBON = "LINT";
    const ACCESSORY_CARD = "KAART";

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var $name
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var $email
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @var $StreetAndNumber
     * @ORM\Column(type="string", length=255)
     */
    private $streetAndNumber;

    /**
     * @var $postalAndCity
     * @ORM\Column(type="string", length=255)
     */
    private $postalAndCity;

    /**
     * @var $phone
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @var $companyNumber
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accessoryType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accessoryText;

    /**
     * @var $invoice
     * @ORM\Column(type="boolean")
     */
    private $invoice;

    /**
     * @var $notes
     * @ORM\Column(type="text", length=65535)
     */
    private $notes;

    /**
     * @var $product
     * @ORM\Column(type="string", length=255)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $productImage;

    /**
     * @ORM\ManyToOne(targetEntity="Comsa\FuneralBundle\Entity\Person", inversedBy="orders")
     */
    private $person;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uuid;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getStreetAndNumber(): ?string
    {
        return $this->streetAndNumber;
    }

    public function setStreetAndNumber(?string $streetAndNumber): void
    {
        $this->streetAndNumber = $streetAndNumber;
    }

    public function getPostalAndCity(): ?string
    {
        return $this->postalAndCity;
    }

    public function setPostalAndCity(?string $postalAndCity): void
    {
        $this->postalAndCity = $postalAndCity;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone)
    {
        $this->phone = $phone;
    }

    public function getCompanyNumber(): ?string
    {
        return $this->companyNumber;
    }

    public function setCompanyNumber(?string $companyNumber): void
    {
        $this->companyNumber = $companyNumber;
    }

    public function getAccessoryType(): ?string
    {
        return $this->accessoryType;
    }

    public function setAccessoryType(?string $accessoryType): void
    {
        $this->accessoryType = $accessoryType;
    }

    public function getAccessoryText(): ?string
    {
        return $this->accessoryText;
    }

    public function setAccessoryText(?string $accessoryText): void
    {
        $this->accessoryText = $accessoryText;
    }

    public function getInvoice(): ?string
    {
        return $this->invoice;
    }


    public function setInvoice(?string $invoice): void
    {
        $this->invoice = $invoice;
    }


    public function getNotes(): ?string
    {
        return $this->notes;
    }


    public function setNotes(?string $notes): void
    {
        $this->notes = $notes;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(?string $product): void
    {
        $this->product = $product;
    }

    public function getProductImage(): ?string
    {
        return $this->productImage;
    }

    public function setProductImage(?string $productImage)
    {
        return $this->productImage = $productImage;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(Person $person): void
    {
        $this->person = $person;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(?string $uuid)
    {
        $this->uuid = $uuid;
    }
}
