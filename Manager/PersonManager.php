<?php

namespace Comsa\FuneralBundle\Manager;

use Comsa\FuneralBundle\Entity\Person;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;


class PersonManager
{
    private $parameterBag;
    private $entityManager;

    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $entityManager)
    {
        $this->parameterBag = $parameterBag;
        $this->entityManager = $entityManager;
    }

    public function getPeople()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->parameterBag->get('comsa_sulu_funeral_bundle_api_link') . "defunt?pompes_funebres_id=4f2fdd56dcc7a7.43283518&days=60",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array('Authorization: ' . $this->parameterBag->get('comsa_sulu_funeral_bundle_auth_key'),),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $data = json_decode($response);

        $people = $data->data->defunts;

        foreach ($people as $person){
            if ($this->entityManager->getRepository(Person::class)->find($person->id)){
                /** @var Person $entity */
                $entity = $this->entityManager->getRepository(Person::class)->find($person->id);
                $entity->setId($person->id);
                if ($person->photo)
                {
                    $entity->setPhoto($this->parameterBag->get('comsa_sulu_funeral_bundle_base_url') . $person->photo);
                }
                $entity->setFirstName($person->prenom);
                $entity->setLastName($person->nom);
                $entity->setSecondName($person->surnom);
                $entity->setFullName($person->nom_complet);
                $entity->setLivestreamUrl($person->livestream_url);
                $entity->setGender($person->genre);
                if ($person->etat_civil !== null ){
                    $entity->setPartnerName($person->etat_civil->nom);
                    $entity->setCivilStatus($person->etat_civil->statut);
                    $entity->setCivilStatusFormat($person->etat_civil->etat_civil_formate);
                }
                if ($person->avis_de_deces){
                    $entity->setMourningLetter($this->parameterBag->get('comsa_sulu_funeral_bundle_base_url') . $person->avis_de_deces);
                }
                $entity->setDateBirth($person->naissance->date);
                $entity->setPlaceBirth($person->naissance->lieu);
                $entity->setDateDeath($person->deces->date);;
                $entity->setTimeDeath($person->deces->heure);
                $entity->setPlaceDeath($person->deces->lieu);
                $entity->setCreatedAt(new \DateTime($person->ajoute_le));
                $entity->setModifiedAt(new \DateTime($person->modifie_le));

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $this->parameterBag->get('comsa_sulu_funeral_bundle_api_link') . "defunt/" . $entity->getId(),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array('Authorization: ' . $this->parameterBag->get('comsa_sulu_funeral_bundle_auth_key'),),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                $data = json_decode($response);

                $entity->setNote(nl2br($data->data->remarque));

                $civilStatusList = [];

                if ($data->data->etat_civil_list)
                {
                    foreach ($data->data->etat_civil_list as $partner)
                    {
                        $civilStatusList[] = $partner->etat_civil_formate;
                    }
                }

                $entity->setCivilStatusList($civilStatusList);

                if ($data->data->derniere_visite !== null)
                {
                    $entity->setFinalVisitDate(new \DateTime($data->data->derniere_visite));
                }

                if ($data->data->option_condoleances->active)
                {
                    $entity->setFamily($data->data->option_condoleances->emails);
                    $entity->setMailFamilyDate(new \DateTime($data->data->option_condoleances->envoye_a_partir_de));
                }


            } else{
                $entity = new Person();
                $entity->setId($person->id);
                $entity->setPhoto($this->parameterBag->get('comsa_sulu_funeral_bundle_base_url') . $person->photo);
                $entity->setFirstName($person->prenom);
                $entity->setLastName($person->nom);
                $entity->setSecondName($person->surnom);
                $entity->setFullName($person->nom_complet);
                $entity->setGender($person->genre);
                if ($person->etat_civil !== null ){
                    $entity->setPartnerName($person->etat_civil->nom);
                    $entity->setCivilStatus($person->etat_civil->statut);
                    $entity->setCivilStatusFormat($person->etat_civil->etat_civil_formate);
                }
                if ($person->avis_de_deces){
                    $entity->setMourningLetter($this->parameterBag->get('comsa_sulu_funeral_bundle_base_url') . $person->avis_de_deces);
                }
                $entity->setLivestreamUrl($person->livestream_url);
                $entity->setDateBirth($person->naissance->date);
                $entity->setPlaceBirth($person->naissance->lieu);
                $entity->setDateDeath($person->deces->date);;
                $entity->setTimeDeath($person->deces->heure);
                $entity->setPlaceDeath($person->deces->lieu);
                $entity->setCreatedAt(new \DateTime($person->ajoute_le));
                $entity->setModifiedAt(new \DateTime($person->modifie_le));

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $this->parameterBag->get('comsa_sulu_funeral_bundle_api_link') . "defunt/" . $entity->getId(),
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array('Authorization: ' . $this->parameterBag->get('comsa_sulu_funeral_bundle_auth_key'),),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);
                $data = json_decode($response);

                $entity->setNote(nl2br($data->data->remarque));

                $civilStatusList = [];

                if ($data->data->etat_civil_list)
                {
                    foreach ($data->data->etat_civil_list as $partner)
                    {
                        $civilStatusList[] = $partner->etat_civil_formate;
                    }
                }

                $entity->setCivilStatusList($civilStatusList);

                if ($data->data->derniere_visite !== null)
                {
                    $entity->setFinalVisitDate(new \DateTime($data->data->derniere_visite));
                }

                if ($data->data->option_condoleances->active)
                {
                    $entity->setFamily($data->data->option_condoleances->emails);
                    $entity->setMailFamilyDate(new \DateTime($data->data->option_condoleances->envoye_a_partir_de));
                }

            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }
}
